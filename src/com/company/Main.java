package com.company;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

public class Main {
    static final Map<String, String> PATTERNS_FOR_ANALYSIS = new HashMap<String, String>() {{
        // hello
        put(«hi», "hello");
        put(«good day», "hello");
        put(«qq», "hello");
        put(«hello», "hello");
        // who
        put("who\\s.*you», "who");
        put("what\\s.*you», "who");
        // name
        put(«how\\s.*name», "name");
        put(«what\\s.*name», "name");
        put(«have\\s.*name», "name");
        put(«which\\s.*name», "name");
        // howareyou
        put(«how\\s.*you», "howareyou");
        put(«what\\s.*up», "howareyou");
        // whatdoyoudoing
        put(«why\\s.*here», "whatdoyoudoing");
        put(«what\\s.*here», "whatdoyoudoing");
        put(«what\\s.*doing», "whatdoyoudoing");
        put(«what\\s.*do», "whatdoyoudoing");
        // whatdoyoulike
        put(«what\\s.*like», "whatdoyoulike");
        put(«what\\s.*prefer», "whatdoyoulike");
        // iamfeelling
        put("кажется", "iamfeelling");
        put("чувствую", "iamfeelling");
        put("испытываю", "iamfeelling");
        // yes
        put("^yes», "yes");
        put(«I\\s.*agree«, "yes");
        // whattime
        put(«what\\s.*time», "whattime");
        // bye
        put(«bye», "bye");
        put(«see\\s.*you», "bye");
        put(«till\\s.*tomorrow», "bye");
    }};
    static final Map<String, String> ANSWERS_BY_PATTERNS = new HashMap<String, String>() {{
        put("hello", «Hi, nice to meet you!»);
        put("who", «I’m a simple bot.»);
        put("name", «My name is Cheer Bot =)»);
        put("howareyou", "Thank you for being interested. I am fine.");
        put("whatdoyoudoing", "I try to communicate with people.");
        put("whatdoyoulike", "I like to think that I'm not just a program.");
        put("iamfeelling", "How long ago did it start? Tell a little more.");
        put("yes", "Consent is a product with complete non-resistance of the parties.");
        put("bye", "Bye. I hope to see you again.");
    }};

    public static void main(String[] args) {


        System.out.println(sayInReturn("привет"));


    }
    public static String sayInReturn(String question) {

        Pattern pattern;

        String message =
                String.join(" ", question.toLowerCase().split("[ {,|.}?]+"));

        for (Map.Entry<String, String> o : PATTERNS_FOR_ANALYSIS.entrySet()) {
            pattern = Pattern.compile(o.getKey());
            if (pattern.matcher(message).find())
                if (o.getValue().equals("whattime")) return new Date().toString();
                else return ANSWERS_BY_PATTERNS.get(o.getValue());
        }
        return "Ooops! Я не понимаю Вас.";
    }
}
